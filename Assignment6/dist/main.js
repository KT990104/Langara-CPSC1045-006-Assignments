/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
let baseString = "TestPage"
function populatePage() {
    let containers = document.querySelectorAll(".container"); //get all the containers

    //Part 1: Rewrite the following code using a for/of loop. 
    //Trivial example: This code capitalizes what is in the HTML file
    //and counts the number of charcters
    //reference for/of loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
    //reference for loops: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
    let count = 0;

    /*
    count = count + containers[0].innerHTML.length;
    containers[0].innerHTML = containers[0].innerHTML.toUpperCase();
    count = count + containers[1].innerHTML.length;
    containers[1].innerHTML = containers[1].innerHTML.toUpperCase();
    count = count + containers[2].innerHTML.length;
    containers[2].innerHTML = containers[2].innerHTML.toUpperCase();
    count = count + containers[3].innerHTML.length;
    containers[3].innerHTML = containers[3].innerHTML.toUpperCase();
    count = count + containers[4].innerHTML.length;
    containers[4].innerHTML = containers[4].innerHTML.toUpperCase();
    count = count + containers[5].innerHTML.length;
    containers[5].innerHTML = containers[5].innerHTML.toUpperCase();
    count = count + containers[6].innerHTML.length;
    containers[6].innerHTML = containers[6].innerHTML.toUpperCase();
    count = count + containers[7].innerHTML.length;
    containers[7].innerHTML = containers[7].innerHTML.toUpperCase();
    */
    for (let i = 0; i < 8; i++){
        count = count + containers[i].innerHTML.length;
         containers[i].innerHTML = containers[i].innerHTML.toUpperCase();
         //output.innerHTML = "Number of characters:" + containers[i];
    }
    //Output Part 1: You do not need to change this.
    let output = document.querySelector("#output");
    output.innerHTML = "Number of characters:" + count;

    //Part 2: Rewrite the following using a loop and a function
    let heights = [4, 5, 3, 7, 6, 10,-2];
    let pointString = "";//start with an empy string
    let x = 0,y = 0;
    
    for(i=0;i<=7;i++) {
            x = x + 10;
            y = 200 - heights[i] * 20;
            pointString = pointString + x + "," + y + " ";
    }
    //Replace the following code with a for/of loop.
    //In this loop, call a function that you will write to 
    //help create the string of points

    /*x = x + 10;
    y = 200 - heights[0] * 20;
    //Take the next line of code and make it a function
    //You function will take in, x, y, and the existing string ( 3 parameters into total)
    //and return the new string with the ponts added.
    //Reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions
    /*pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[1] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[2] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[3] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[4] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[5] * 20;
    pointString = pointString + x + "," + y + " ";

    x = x + 10;
    y = 200 - heights[6] * 20;
    pointString = pointString + x + "," + y + " ";*/
    let output2 = document.querySelector("#output2");
    output2.setAttribute("points", pointString);
    


    //Part 3:  Creating things with loop
    // on the html page with loop.
    //mananually, there are other ways of course.
    let data = [30, 70, 110, 150];
    let svgString = '<svg width="500" height="500">';
    x= 0;
    for(i=0;i<=4;i++){
        x = data[i];
        circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />'
        svgString += circleString;
    }

    /*
    x = data[0];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[1];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[2];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;

    x = data[3];
    circleString = '<circle cx="'+x+'"'+' cy="'+250+'" r="'+20+'" fill="black" />';
    svgString += circleString;
    */
    svgString += "</svg>";
    let output3 = document.querySelector("#output3");
    output3.innerHTML += svgString;

}



/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2REFBNkQ7O0FBRTdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsT0FBTztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCOztBQUVBLFlBQVksS0FBSztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxrREFBa0Q7QUFDbEQ7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVksS0FBSztBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsIi8vVGhpcyBpcyB0aGUgZW50cnkgcG9pbnQgSmF2YVNjcmlwdCBmaWxlLlxyXG4vL1dlYnBhY2sgd2lsbCBsb29rIGF0IHRoaXMgZmlsZSBmaXJzdCwgYW5kIHRoZW4gY2hlY2tcclxuLy93aGF0IGZpbGVzIGFyZSBsaW5rZWQgdG8gaXQuXHJcbmNvbnNvbGUubG9nKFwiSGVsbG8gd29ybGRcIik7XHJcbmxldCBiYXNlU3RyaW5nID0gXCJUZXN0UGFnZVwiXHJcbmZ1bmN0aW9uIHBvcHVsYXRlUGFnZSgpIHtcclxuICAgIGxldCBjb250YWluZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5jb250YWluZXJcIik7IC8vZ2V0IGFsbCB0aGUgY29udGFpbmVyc1xyXG5cclxuICAgIC8vUGFydCAxOiBSZXdyaXRlIHRoZSBmb2xsb3dpbmcgY29kZSB1c2luZyBhIGZvci9vZiBsb29wLiBcclxuICAgIC8vVHJpdmlhbCBleGFtcGxlOiBUaGlzIGNvZGUgY2FwaXRhbGl6ZXMgd2hhdCBpcyBpbiB0aGUgSFRNTCBmaWxlXHJcbiAgICAvL2FuZCBjb3VudHMgdGhlIG51bWJlciBvZiBjaGFyY3RlcnNcclxuICAgIC8vcmVmZXJlbmNlIGZvci9vZiBsb29wczogaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9XZWIvSmF2YVNjcmlwdC9SZWZlcmVuY2UvU3RhdGVtZW50cy9mb3IuLi5vZlxyXG4gICAgLy9yZWZlcmVuY2UgZm9yIGxvb3BzOiBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9KYXZhU2NyaXB0L1JlZmVyZW5jZS9TdGF0ZW1lbnRzL2ZvclxyXG4gICAgbGV0IGNvdW50ID0gMDtcclxuXHJcbiAgICAvKlxyXG4gICAgY291bnQgPSBjb3VudCArIGNvbnRhaW5lcnNbMF0uaW5uZXJIVE1MLmxlbmd0aDtcclxuICAgIGNvbnRhaW5lcnNbMF0uaW5uZXJIVE1MID0gY29udGFpbmVyc1swXS5pbm5lckhUTUwudG9VcHBlckNhc2UoKTtcclxuICAgIGNvdW50ID0gY291bnQgKyBjb250YWluZXJzWzFdLmlubmVySFRNTC5sZW5ndGg7XHJcbiAgICBjb250YWluZXJzWzFdLmlubmVySFRNTCA9IGNvbnRhaW5lcnNbMV0uaW5uZXJIVE1MLnRvVXBwZXJDYXNlKCk7XHJcbiAgICBjb3VudCA9IGNvdW50ICsgY29udGFpbmVyc1syXS5pbm5lckhUTUwubGVuZ3RoO1xyXG4gICAgY29udGFpbmVyc1syXS5pbm5lckhUTUwgPSBjb250YWluZXJzWzJdLmlubmVySFRNTC50b1VwcGVyQ2FzZSgpO1xyXG4gICAgY291bnQgPSBjb3VudCArIGNvbnRhaW5lcnNbM10uaW5uZXJIVE1MLmxlbmd0aDtcclxuICAgIGNvbnRhaW5lcnNbM10uaW5uZXJIVE1MID0gY29udGFpbmVyc1szXS5pbm5lckhUTUwudG9VcHBlckNhc2UoKTtcclxuICAgIGNvdW50ID0gY291bnQgKyBjb250YWluZXJzWzRdLmlubmVySFRNTC5sZW5ndGg7XHJcbiAgICBjb250YWluZXJzWzRdLmlubmVySFRNTCA9IGNvbnRhaW5lcnNbNF0uaW5uZXJIVE1MLnRvVXBwZXJDYXNlKCk7XHJcbiAgICBjb3VudCA9IGNvdW50ICsgY29udGFpbmVyc1s1XS5pbm5lckhUTUwubGVuZ3RoO1xyXG4gICAgY29udGFpbmVyc1s1XS5pbm5lckhUTUwgPSBjb250YWluZXJzWzVdLmlubmVySFRNTC50b1VwcGVyQ2FzZSgpO1xyXG4gICAgY291bnQgPSBjb3VudCArIGNvbnRhaW5lcnNbNl0uaW5uZXJIVE1MLmxlbmd0aDtcclxuICAgIGNvbnRhaW5lcnNbNl0uaW5uZXJIVE1MID0gY29udGFpbmVyc1s2XS5pbm5lckhUTUwudG9VcHBlckNhc2UoKTtcclxuICAgIGNvdW50ID0gY291bnQgKyBjb250YWluZXJzWzddLmlubmVySFRNTC5sZW5ndGg7XHJcbiAgICBjb250YWluZXJzWzddLmlubmVySFRNTCA9IGNvbnRhaW5lcnNbN10uaW5uZXJIVE1MLnRvVXBwZXJDYXNlKCk7XHJcbiAgICAqL1xyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCA4OyBpKyspe1xyXG4gICAgICAgIGNvdW50ID0gY291bnQgKyBjb250YWluZXJzW2ldLmlubmVySFRNTC5sZW5ndGg7XHJcbiAgICAgICAgIGNvbnRhaW5lcnNbaV0uaW5uZXJIVE1MID0gY29udGFpbmVyc1tpXS5pbm5lckhUTUwudG9VcHBlckNhc2UoKTtcclxuICAgICAgICAgLy9vdXRwdXQuaW5uZXJIVE1MID0gXCJOdW1iZXIgb2YgY2hhcmFjdGVyczpcIiArIGNvbnRhaW5lcnNbaV07XHJcbiAgICB9XHJcbiAgICAvL091dHB1dCBQYXJ0IDE6IFlvdSBkbyBub3QgbmVlZCB0byBjaGFuZ2UgdGhpcy5cclxuICAgIGxldCBvdXRwdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI291dHB1dFwiKTtcclxuICAgIG91dHB1dC5pbm5lckhUTUwgPSBcIk51bWJlciBvZiBjaGFyYWN0ZXJzOlwiICsgY291bnQ7XHJcblxyXG4gICAgLy9QYXJ0IDI6IFJld3JpdGUgdGhlIGZvbGxvd2luZyB1c2luZyBhIGxvb3AgYW5kIGEgZnVuY3Rpb25cclxuICAgIGxldCBoZWlnaHRzID0gWzQsIDUsIDMsIDcsIDYsIDEwLC0yXTtcclxuICAgIGxldCBwb2ludFN0cmluZyA9IFwiXCI7Ly9zdGFydCB3aXRoIGFuIGVtcHkgc3RyaW5nXHJcbiAgICBsZXQgeCA9IDAseSA9IDA7XHJcbiAgICBcclxuICAgIGZvcihpPTA7aTw9NztpKyspIHtcclxuICAgICAgICAgICAgeCA9IHggKyAxMDtcclxuICAgICAgICAgICAgeSA9IDIwMCAtIGhlaWdodHNbaV0gKiAyMDtcclxuICAgICAgICAgICAgcG9pbnRTdHJpbmcgPSBwb2ludFN0cmluZyArIHggKyBcIixcIiArIHkgKyBcIiBcIjtcclxuICAgIH1cclxuICAgIC8vUmVwbGFjZSB0aGUgZm9sbG93aW5nIGNvZGUgd2l0aCBhIGZvci9vZiBsb29wLlxyXG4gICAgLy9JbiB0aGlzIGxvb3AsIGNhbGwgYSBmdW5jdGlvbiB0aGF0IHlvdSB3aWxsIHdyaXRlIHRvIFxyXG4gICAgLy9oZWxwIGNyZWF0ZSB0aGUgc3RyaW5nIG9mIHBvaW50c1xyXG5cclxuICAgIC8qeCA9IHggKyAxMDtcclxuICAgIHkgPSAyMDAgLSBoZWlnaHRzWzBdICogMjA7XHJcbiAgICAvL1Rha2UgdGhlIG5leHQgbGluZSBvZiBjb2RlIGFuZCBtYWtlIGl0IGEgZnVuY3Rpb25cclxuICAgIC8vWW91IGZ1bmN0aW9uIHdpbGwgdGFrZSBpbiwgeCwgeSwgYW5kIHRoZSBleGlzdGluZyBzdHJpbmcgKCAzIHBhcmFtZXRlcnMgaW50byB0b3RhbClcclxuICAgIC8vYW5kIHJldHVybiB0aGUgbmV3IHN0cmluZyB3aXRoIHRoZSBwb250cyBhZGRlZC5cclxuICAgIC8vUmVmZXJlbmNlOiBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9KYXZhU2NyaXB0L0d1aWRlL0Z1bmN0aW9uc1xyXG4gICAgLypwb2ludFN0cmluZyA9IHBvaW50U3RyaW5nICsgeCArIFwiLFwiICsgeSArIFwiIFwiO1xyXG5cclxuICAgIHggPSB4ICsgMTA7XHJcbiAgICB5ID0gMjAwIC0gaGVpZ2h0c1sxXSAqIDIwO1xyXG4gICAgcG9pbnRTdHJpbmcgPSBwb2ludFN0cmluZyArIHggKyBcIixcIiArIHkgKyBcIiBcIjtcclxuXHJcbiAgICB4ID0geCArIDEwO1xyXG4gICAgeSA9IDIwMCAtIGhlaWdodHNbMl0gKiAyMDtcclxuICAgIHBvaW50U3RyaW5nID0gcG9pbnRTdHJpbmcgKyB4ICsgXCIsXCIgKyB5ICsgXCIgXCI7XHJcblxyXG4gICAgeCA9IHggKyAxMDtcclxuICAgIHkgPSAyMDAgLSBoZWlnaHRzWzNdICogMjA7XHJcbiAgICBwb2ludFN0cmluZyA9IHBvaW50U3RyaW5nICsgeCArIFwiLFwiICsgeSArIFwiIFwiO1xyXG5cclxuICAgIHggPSB4ICsgMTA7XHJcbiAgICB5ID0gMjAwIC0gaGVpZ2h0c1s0XSAqIDIwO1xyXG4gICAgcG9pbnRTdHJpbmcgPSBwb2ludFN0cmluZyArIHggKyBcIixcIiArIHkgKyBcIiBcIjtcclxuXHJcbiAgICB4ID0geCArIDEwO1xyXG4gICAgeSA9IDIwMCAtIGhlaWdodHNbNV0gKiAyMDtcclxuICAgIHBvaW50U3RyaW5nID0gcG9pbnRTdHJpbmcgKyB4ICsgXCIsXCIgKyB5ICsgXCIgXCI7XHJcblxyXG4gICAgeCA9IHggKyAxMDtcclxuICAgIHkgPSAyMDAgLSBoZWlnaHRzWzZdICogMjA7XHJcbiAgICBwb2ludFN0cmluZyA9IHBvaW50U3RyaW5nICsgeCArIFwiLFwiICsgeSArIFwiIFwiOyovXHJcbiAgICBsZXQgb3V0cHV0MiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjb3V0cHV0MlwiKTtcclxuICAgIG91dHB1dDIuc2V0QXR0cmlidXRlKFwicG9pbnRzXCIsIHBvaW50U3RyaW5nKTtcclxuICAgIFxyXG5cclxuXHJcbiAgICAvL1BhcnQgMzogIENyZWF0aW5nIHRoaW5ncyB3aXRoIGxvb3BcclxuICAgIC8vIG9uIHRoZSBodG1sIHBhZ2Ugd2l0aCBsb29wLlxyXG4gICAgLy9tYW5hbnVhbGx5LCB0aGVyZSBhcmUgb3RoZXIgd2F5cyBvZiBjb3Vyc2UuXHJcbiAgICBsZXQgZGF0YSA9IFszMCwgNzAsIDExMCwgMTUwXTtcclxuICAgIGxldCBzdmdTdHJpbmcgPSAnPHN2ZyB3aWR0aD1cIjUwMFwiIGhlaWdodD1cIjUwMFwiPic7XHJcbiAgICB4PSAwO1xyXG4gICAgZm9yKGk9MDtpPD00O2krKyl7XHJcbiAgICAgICAgeCA9IGRhdGFbaV07XHJcbiAgICAgICAgY2lyY2xlU3RyaW5nID0gJzxjaXJjbGUgY3g9XCInK3grJ1wiJysnIGN5PVwiJysyNTArJ1wiIHI9XCInKzIwKydcIiBmaWxsPVwiYmxhY2tcIiAvPidcclxuICAgICAgICBzdmdTdHJpbmcgKz0gY2lyY2xlU3RyaW5nO1xyXG4gICAgfVxyXG5cclxuICAgIC8qXHJcbiAgICB4ID0gZGF0YVswXTtcclxuICAgIGNpcmNsZVN0cmluZyA9ICc8Y2lyY2xlIGN4PVwiJyt4KydcIicrJyBjeT1cIicrMjUwKydcIiByPVwiJysyMCsnXCIgZmlsbD1cImJsYWNrXCIgLz4nO1xyXG4gICAgc3ZnU3RyaW5nICs9IGNpcmNsZVN0cmluZztcclxuXHJcbiAgICB4ID0gZGF0YVsxXTtcclxuICAgIGNpcmNsZVN0cmluZyA9ICc8Y2lyY2xlIGN4PVwiJyt4KydcIicrJyBjeT1cIicrMjUwKydcIiByPVwiJysyMCsnXCIgZmlsbD1cImJsYWNrXCIgLz4nO1xyXG4gICAgc3ZnU3RyaW5nICs9IGNpcmNsZVN0cmluZztcclxuXHJcbiAgICB4ID0gZGF0YVsyXTtcclxuICAgIGNpcmNsZVN0cmluZyA9ICc8Y2lyY2xlIGN4PVwiJyt4KydcIicrJyBjeT1cIicrMjUwKydcIiByPVwiJysyMCsnXCIgZmlsbD1cImJsYWNrXCIgLz4nO1xyXG4gICAgc3ZnU3RyaW5nICs9IGNpcmNsZVN0cmluZztcclxuXHJcbiAgICB4ID0gZGF0YVszXTtcclxuICAgIGNpcmNsZVN0cmluZyA9ICc8Y2lyY2xlIGN4PVwiJyt4KydcIicrJyBjeT1cIicrMjUwKydcIiByPVwiJysyMCsnXCIgZmlsbD1cImJsYWNrXCIgLz4nO1xyXG4gICAgc3ZnU3RyaW5nICs9IGNpcmNsZVN0cmluZztcclxuICAgICovXHJcbiAgICBzdmdTdHJpbmcgKz0gXCI8L3N2Zz5cIjtcclxuICAgIGxldCBvdXRwdXQzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNvdXRwdXQzXCIpO1xyXG4gICAgb3V0cHV0My5pbm5lckhUTUwgKz0gc3ZnU3RyaW5nO1xyXG5cclxufVxyXG5cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==
//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("I am Home")
function sliderXlistener(){
    let slider = document.querySelector("#rangeX");
    let circle = document.querySelector("#rightcir");
    let circle2 = document.querySelector("#leftcir");
    circle.setAttribute("cx",slider.value);
    circle2.setAttribute("cx",250+(250-Number(slider.value)));
    console.log(circle);

}
function sliderYlistener(){
    let slider = document.querySelector("#rangeY");
    let circle3 = document.querySelector("#rightcir");
    let circle4 = document.querySelector("#leftcir");
    circle4.setAttribute("r",slider.value);
    circle3.setAttribute("r",100+(100-Number(slider.value)));
    console.log(circle);

}
window.sliderXlistener = sliderXlistener;
window.sliderYlistener = sliderYlistener;


/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

//depending on the change of radii I want to change values of points of the polygon 
function changeRadii() {
    //empty function 

    //1. get ro and ri from input 
    let ri = document.querySelector("#ri");
    let riValue = Number(ri.value); 
    let ro = document.querySelector("#ro")
    let roValue = Number(ro.value)//this is how you get the value
    let polygon = document.querySelector("#star"); 
    //...

    //do the math 
    let p1x = roValue * Math.cos(0);
    let p1y = roValue * Math.sin(0);
    let p2x = riValue * Math.cos(0.76);
    let p2y = riValue * Math.sin(0.76);
    let p3x = roValue * Math.cos(1.75);
    let p3y = roValue * Math.sin(1.75);
    let p4x = riValue * Math.cos(2.36);
    let p4y = riValue * Math.sin(2.36);
    let p5x = roValue * Math.cos(3.14);
    let p5y = roValue * Math.sin(3.14);
    let p6x = riValue * Math.cos(3.92);
    let p6y = riValue * Math.sin(3.92);
    let p7x = roValue * Math.cos(4.71);
    let p7y = roValue * Math.sin(4.71);
    let p8x = riValue * Math.cos(5.50);
    let p8y = riValue * Math.sin(5.50);

console.log(p1x);
console.log(p1y);
console.log(p2x);
console.log(p2y);
console.log(p3x);
console.log(p3y);
console.log(p4x);
console.log(p4y);
console.log(p5x);
console.log(p5y);
console.log(p6x);
console.log(p6y);
console.log(p7x);
console.log(p7y);
console.log(p8x);
console.log(p8y);
 
    let pointString=p1x +"," +p1y+" " + p2x +","+ p2y +" "+ p3x +"," + p3y +" " + p4x +","+ p4y +" " +  p5x +","+ p5y +" "+ p6x +","+ p6y+" " + p7x +","+ p7y +" " + p8x+"," + p8y+" " ;
    //let polygon="p1x,p1y p2x,p2y p3x,p3y p4x,p4y p5x,p5y p6x,p6y p7x,p7y p8x,p8y";
    polygon.setAttribute("points", pointString)

    
   

}

window.onload = changeRadii;
window.changeRadii = changeRadii;


function sliderXlistener(){
    let sliderX = document.querySelector("#X");
        let sliderY = document.querySelector("#Y");
     let collection = document.querySelector("#collection");
     let transformString = "transform("+20+" "+20+")";
     
collection.setAttribute("transform",transformString);
}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1DO0FBQ0E7QUFDQTtBQUNBLGtEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7O0FBRUE7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEMiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiLy9UaGlzIGlzIHRoZSBlbnRyeSBwb2ludCBKYXZhU2NyaXB0IGZpbGUuXHJcbi8vV2VicGFjayB3aWxsIGxvb2sgYXQgdGhpcyBmaWxlIGZpcnN0LCBhbmQgdGhlbiBjaGVja1xyXG4vL3doYXQgZmlsZXMgYXJlIGxpbmtlZCB0byBpdC5cclxuY29uc29sZS5sb2coXCJIZWxsbyB3b3JsZFwiKTtcclxuXHJcbi8vZGVwZW5kaW5nIG9uIHRoZSBjaGFuZ2Ugb2YgcmFkaWkgSSB3YW50IHRvIGNoYW5nZSB2YWx1ZXMgb2YgcG9pbnRzIG9mIHRoZSBwb2x5Z29uIFxyXG5mdW5jdGlvbiBjaGFuZ2VSYWRpaSgpIHtcclxuICAgIC8vZW1wdHkgZnVuY3Rpb24gXHJcblxyXG4gICAgLy8xLiBnZXQgcm8gYW5kIHJpIGZyb20gaW5wdXQgXHJcbiAgICBsZXQgcmkgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3JpXCIpO1xyXG4gICAgbGV0IHJpVmFsdWUgPSBOdW1iZXIocmkudmFsdWUpOyBcclxuICAgIGxldCBybyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcm9cIilcclxuICAgIGxldCByb1ZhbHVlID0gTnVtYmVyKHJvLnZhbHVlKS8vdGhpcyBpcyBob3cgeW91IGdldCB0aGUgdmFsdWVcclxuICAgIGxldCBwb2x5Z29uID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzdGFyXCIpOyBcclxuICAgIC8vLi4uXHJcblxyXG4gICAgLy9kbyB0aGUgbWF0aCBcclxuICAgIGxldCBwMXggPSByb1ZhbHVlICogTWF0aC5jb3MoMCk7XHJcbiAgICBsZXQgcDF5ID0gcm9WYWx1ZSAqIE1hdGguc2luKDApO1xyXG4gICAgbGV0IHAyeCA9IHJpVmFsdWUgKiBNYXRoLmNvcygwLjc2KTtcclxuICAgIGxldCBwMnkgPSByaVZhbHVlICogTWF0aC5zaW4oMC43Nik7XHJcbiAgICBsZXQgcDN4ID0gcm9WYWx1ZSAqIE1hdGguY29zKDEuNzUpO1xyXG4gICAgbGV0IHAzeSA9IHJvVmFsdWUgKiBNYXRoLnNpbigxLjc1KTtcclxuICAgIGxldCBwNHggPSByaVZhbHVlICogTWF0aC5jb3MoMi4zNik7XHJcbiAgICBsZXQgcDR5ID0gcmlWYWx1ZSAqIE1hdGguc2luKDIuMzYpO1xyXG4gICAgbGV0IHA1eCA9IHJvVmFsdWUgKiBNYXRoLmNvcygzLjE0KTtcclxuICAgIGxldCBwNXkgPSByb1ZhbHVlICogTWF0aC5zaW4oMy4xNCk7XHJcbiAgICBsZXQgcDZ4ID0gcmlWYWx1ZSAqIE1hdGguY29zKDMuOTIpO1xyXG4gICAgbGV0IHA2eSA9IHJpVmFsdWUgKiBNYXRoLnNpbigzLjkyKTtcclxuICAgIGxldCBwN3ggPSByb1ZhbHVlICogTWF0aC5jb3MoNC43MSk7XHJcbiAgICBsZXQgcDd5ID0gcm9WYWx1ZSAqIE1hdGguc2luKDQuNzEpO1xyXG4gICAgbGV0IHA4eCA9IHJpVmFsdWUgKiBNYXRoLmNvcyg1LjUwKTtcclxuICAgIGxldCBwOHkgPSByaVZhbHVlICogTWF0aC5zaW4oNS41MCk7XHJcblxyXG5jb25zb2xlLmxvZyhwMXgpO1xyXG5jb25zb2xlLmxvZyhwMXkpO1xyXG5jb25zb2xlLmxvZyhwMngpO1xyXG5jb25zb2xlLmxvZyhwMnkpO1xyXG5jb25zb2xlLmxvZyhwM3gpO1xyXG5jb25zb2xlLmxvZyhwM3kpO1xyXG5jb25zb2xlLmxvZyhwNHgpO1xyXG5jb25zb2xlLmxvZyhwNHkpO1xyXG5jb25zb2xlLmxvZyhwNXgpO1xyXG5jb25zb2xlLmxvZyhwNXkpO1xyXG5jb25zb2xlLmxvZyhwNngpO1xyXG5jb25zb2xlLmxvZyhwNnkpO1xyXG5jb25zb2xlLmxvZyhwN3gpO1xyXG5jb25zb2xlLmxvZyhwN3kpO1xyXG5jb25zb2xlLmxvZyhwOHgpO1xyXG5jb25zb2xlLmxvZyhwOHkpO1xyXG4gXHJcbiAgICBsZXQgcG9pbnRTdHJpbmc9cDF4ICtcIixcIiArcDF5K1wiIFwiICsgcDJ4ICtcIixcIisgcDJ5ICtcIiBcIisgcDN4ICtcIixcIiArIHAzeSArXCIgXCIgKyBwNHggK1wiLFwiKyBwNHkgK1wiIFwiICsgIHA1eCArXCIsXCIrIHA1eSArXCIgXCIrIHA2eCArXCIsXCIrIHA2eStcIiBcIiArIHA3eCArXCIsXCIrIHA3eSArXCIgXCIgKyBwOHgrXCIsXCIgKyBwOHkrXCIgXCIgO1xyXG4gICAgLy9sZXQgcG9seWdvbj1cInAxeCxwMXkgcDJ4LHAyeSBwM3gscDN5IHA0eCxwNHkgcDV4LHA1eSBwNngscDZ5IHA3eCxwN3kgcDh4LHA4eVwiO1xyXG4gICAgcG9seWdvbi5zZXRBdHRyaWJ1dGUoXCJwb2ludHNcIiwgcG9pbnRTdHJpbmcpXHJcblxyXG4gICAgXHJcbiAgIFxyXG5cclxufVxyXG5cclxud2luZG93Lm9ubG9hZCA9IGNoYW5nZVJhZGlpO1xyXG53aW5kb3cuY2hhbmdlUmFkaWkgPSBjaGFuZ2VSYWRpaTtcclxuXHJcblxyXG5mdW5jdGlvbiBzbGlkZXJYbGlzdGVuZXIoKXtcclxuICAgIGxldCBzbGlkZXJYID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNYXCIpO1xyXG4gICAgICAgIGxldCBzbGlkZXJZID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNZXCIpO1xyXG4gICAgIGxldCBjb2xsZWN0aW9uID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNjb2xsZWN0aW9uXCIpO1xyXG4gICAgIGxldCB0cmFuc2Zvcm1TdHJpbmcgPSBcInRyYW5zZm9ybShcIisyMCtcIiBcIisyMCtcIilcIjtcclxuICAgICBcclxuY29sbGVjdGlvbi5zZXRBdHRyaWJ1dGUoXCJ0cmFuc2Zvcm1cIix0cmFuc2Zvcm1TdHJpbmcpO1xyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==
//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

//depending on the change of radii I want to change values of points of the polygon 
function changeRadii() {
    //empty function 

    //1. get ro and ri from input 
    let ri = document.querySelector("#ri");
    let riValue = Number(ri.value); 
    let ro = document.querySelector("#ro")
    let roValue = Number(ro.value)//this is how you get the value
    let polygon = document.querySelector("#star"); 
    //...

    //do the math 
    let p1x = roValue * Math.cos(0);
    let p1y = roValue * Math.sin(0);
    let p2x = riValue * Math.cos(0.76);
    let p2y = riValue * Math.sin(0.76);
    let p3x = roValue * Math.cos(1.75);
    let p3y = roValue * Math.sin(1.75);
    let p4x = riValue * Math.cos(2.36);
    let p4y = riValue * Math.sin(2.36);
    let p5x = roValue * Math.cos(3.14);
    let p5y = roValue * Math.sin(3.14);
    let p6x = riValue * Math.cos(3.92);
    let p6y = riValue * Math.sin(3.92);
    let p7x = roValue * Math.cos(4.71);
    let p7y = roValue * Math.sin(4.71);
    let p8x = riValue * Math.cos(5.50);
    let p8y = riValue * Math.sin(5.50);

console.log(p1x);
console.log(p1y);
console.log(p2x);
console.log(p2y);
console.log(p3x);
console.log(p3y);
console.log(p4x);
console.log(p4y);
console.log(p5x);
console.log(p5y);
console.log(p6x);
console.log(p6y);
console.log(p7x);
console.log(p7y);
console.log(p8x);
console.log(p8y);
 
    let pointString=p1x +"," +p1y+" " + p2x +","+ p2y +" "+ p3x +"," + p3y +" " + p4x +","+ p4y +" " +  p5x +","+ p5y +" "+ p6x +","+ p6y+" " + p7x +","+ p7y +" " + p8x+"," + p8y+" " ;
    //let polygon="p1x,p1y p2x,p2y p3x,p3y p4x,p4y p5x,p5y p6x,p6y p7x,p7y p8x,p8y";
    polygon.setAttribute("points", pointString)

    
   

}

window.onload = changeRadii;
window.changeRadii = changeRadii;


function sliderXlistener(){
    //let sliderX = document.querySelector("#X");
    var val = document.getElementById("X").value;
    //let sliderY = document.querySelector("#Y");
    let collection = document.querySelector("#collection");
    let translateString = "translate("+val+" "+500+")";
    collection.setAttribute("transform",translateString);
}
function sliderYlistener(){
    //let sliderY = document.querySelector("#Y");
    var val = document.getElementById("Y").value;
    let collection = document.querySelector("#collection");
    let translateString = "translate("+500+" "+val+")";
    collection.setAttribute("transform",translateString);
}
window.sliderXlistener = sliderXlistener;
window.sliderYlistener = sliderYlistener;